import { BrowserRouter, Routes, Route } from "react-router-dom";
import Register from "./Register";


export default function Routing() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Page/>}>
          <Route index element={<Main/>} />
          <Route path="register" element={<Register />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
